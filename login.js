document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault(); // Mencegah form untuk disubmit

    // Mengambil nilai input
    var nama = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var sandi = document.getElementById('pass').value;

    // Memeriksa jika nilai input kosong
    if (nama.trim() === '' || email.trim() === '' || sandi.trim() === '') {
        alert('Harap isi semua kolom');
        return;
    }

    // Memeriksa email valid
    if (!validateEmail(email)) {
        alert('Harap masukkan alamat email yang valid');
        return;
    }

    // Memeriksa panjang password
    if (sandi.length < 6) {
        alert('Panjang password minimal 6 karakter');
        return;
    }

    // Menampilkan alert dengan informasi login
    alert('Login berhasil! \nUsername: ' + nama + '\nEmail: ' + email);
});

// Fungsi untuk memeriksa email valid
function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
